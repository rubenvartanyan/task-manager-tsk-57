package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ISessionRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ITaskRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.api.service.model.ISessionServiceGraph;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.system.*;
import ru.vartanyan.tm.model.SessionGraph;
import ru.vartanyan.tm.model.UserGraph;
import ru.vartanyan.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class SessionServiceGraph extends AbstractServiceGraph<SessionGraph>
        implements ISessionServiceGraph {

    @NotNull
    public ISessionRepositoryGraph getSessionRepositoryGraph() {
        return context.getBean(ISessionRepositoryGraph.class);
    }

    @NotNull
    @Autowired
    private IUserServiceGraph userServiceGraph;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    public void add(@Nullable final SessionGraph session) {
        if (session == null) throw new NullObjectException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        try {
            sessionRepositoryGraph.begin();
            sessionRepositoryGraph.add(session);
            sessionRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            sessionRepositoryGraph.rollback();
            throw e;
        } finally {
            sessionRepositoryGraph.close();
        }
    }

    @SneakyThrows
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || password == null) return false;
        final @NotNull UserGraph user = userServiceGraph.findByLogin(login);
        if (user.getLocked()) throw new UserLockedException();
        final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionGraph open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        final @NotNull UserGraph user = userServiceGraph.findByLogin(login);
        @NotNull final SessionGraph session = new SessionGraph();
        session.setUser(user);
        @Nullable final SessionGraph signSession = sign(session);
        if (signSession == null) return null;
        try {
            sessionRepositoryGraph.begin();
            sessionRepositoryGraph.add(signSession);
            sessionRepositoryGraph.commit();
            return signSession;
        } catch (@NotNull final Exception e) {
            sessionRepositoryGraph.rollback();
            throw e;
        } finally {
            sessionRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null) throw new AccessDeniedException();
        if (session.getUser() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionGraph sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        if (sessionRepositoryGraph.findOneById(session.getId()) == null) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionGraph session,
                              @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (session.getUser().getId() == null) throw new AccessDeniedException();
        validate(session);
        final @NotNull UserGraph user = userServiceGraph.findOneById(session.getUser().getId());
        if (user.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    @SneakyThrows
    public SessionGraph close(@Nullable final SessionGraph session) {
        if (session == null) return null;
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        try {
            sessionRepositoryGraph.begin();
            sessionRepositoryGraph.removeOneById(session.getId());
            sessionRepositoryGraph.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sessionRepositoryGraph.rollback();
            throw e;
        } finally {
            sessionRepositoryGraph.close();
        }
    }

    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<SessionGraph> entities) {
        if (entities == null) throw new NullObjectException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        try {
            sessionRepositoryGraph.begin();
            entities.forEach(sessionRepositoryGraph::add);
            sessionRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            sessionRepositoryGraph.rollback();
            throw e;
        } finally {
            sessionRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        try {
            sessionRepositoryGraph.begin();
            sessionRepositoryGraph.clear();
            sessionRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            sessionRepositoryGraph.rollback();
            throw e;
        } finally {
            sessionRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(final @Nullable SessionGraph entity) {
        if (entity == null) throw new NullObjectException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        try {
            sessionRepositoryGraph.begin();
            sessionRepositoryGraph.removeOneById(entity.getId());
            sessionRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            sessionRepositoryGraph.rollback();
            throw e;
        } finally {
            sessionRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public @Nullable List<SessionGraph> findAll() {
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        try {
            return sessionRepositoryGraph.findAll();
        } finally {
            sessionRepositoryGraph.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionGraph findOneById(
            @Nullable final String id
    ) {
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        if (id == null) throw new EmptyIdException();
        try {
            return sessionRepositoryGraph.findOneById(id);
        } finally {
            sessionRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        ISessionRepositoryGraph sessionRepositoryGraph = getSessionRepositoryGraph();
        try {
            sessionRepositoryGraph.begin();
            sessionRepositoryGraph.removeOneById(id);
            sessionRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            sessionRepositoryGraph.rollback();
            throw e;
        } finally {
            sessionRepositoryGraph.close();
        }
    }

}
