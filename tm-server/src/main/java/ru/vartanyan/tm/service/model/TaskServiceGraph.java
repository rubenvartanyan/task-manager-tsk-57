package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ISessionRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.ITaskRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.ITaskServiceGraph;
import ru.vartanyan.tm.api.service.model.IUserServiceGraph;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.TaskGraph;
import ru.vartanyan.tm.repository.model.TaskRepositoryGraph;
import ru.vartanyan.tm.repository.model.UserRepositoryGraph;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class TaskServiceGraph extends AbstractServiceGraph<TaskGraph>
        implements ITaskServiceGraph {

    @NotNull
    public IUserRepositoryGraph getUserRepositoryGraph() {
        return context.getBean(IUserRepositoryGraph.class);
    }

    @NotNull
    public ITaskRepositoryGraph getTaskRepositoryGraph() {
        return context.getBean(ITaskRepositoryGraph.class);
    }

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @SneakyThrows
    @Override
    public @NotNull TaskGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final TaskGraph task = new TaskGraph();
        task.setName(name);
        task.setDescription(description);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        IUserRepositoryGraph userRepositoryGraph = getUserRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            task.setUser(userRepositoryGraph.findOneById(userId));
            taskRepositoryGraph.add(task);
            taskRepositoryGraph.commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final TaskGraph task) {
        if (task == null) throw new NullObjectException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.add(task);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<TaskGraph> entities) {
        if (entities == null) throw new NullObjectException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            entities.forEach(taskRepositoryGraph::add);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.clear();
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(final @Nullable TaskGraph entity) {
        if (entity == null) throw new NullObjectException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.removeOneById(entity.getId());
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @Override
    @SneakyThrows
    public @Nullable List<TaskGraph> findAll() {
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            return taskRepositoryGraph.findAll();
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findOneById(
            @Nullable final String id
    ) {
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        if (id == null) throw new EmptyIdException();
        try {
            return taskRepositoryGraph.findOneById(id);
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyLoginException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.removeOneById(id);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.clearByUserId(userId);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public @NotNull List<TaskGraph> findAll(@NotNull final String userId) {
        if (userId == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            return taskRepositoryGraph.findAllByUserId(userId);
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public @NotNull TaskGraph findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            return taskRepositoryGraph.findOneByIdAndUserId(userId, id);
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public @Nullable TaskGraph findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            return taskRepositoryGraph.findOneByIndex(userId, index);
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public @Nullable TaskGraph findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            return taskRepositoryGraph.findOneByName(userId, name);
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void remove(
            @Nullable final String userId,
            @Nullable final TaskGraph entity
    ) {
        if (userId == null) throw new EmptyIdException();
        if (entity == null) throw new NullObjectException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.removeOneByIdAndUserId(userId, entity.getId());
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.removeOneByIdAndUserId(userId, id);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            @Nullable TaskGraph task = taskRepositoryGraph.findOneByIndex(userId, index);
            taskRepositoryGraph.removeOneByIdAndUserId(userId, task.getId());
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.removeOneByName(userId, name);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final TaskGraph entity = findOneById(userId, id);
        entity.setStatus(status);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final TaskGraph entity = findOneByIndex(userId, index);
        entity.setStatus(status);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (status == null) throw new NullObjectException();
        @NotNull final TaskGraph entity = findOneByName(userId, name);
        entity.setStatus(status);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final TaskGraph entity = findOneById(userId, id);
        entity.setStatus(Status.COMPLETE);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final TaskGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.COMPLETE);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final TaskGraph entity = findOneByName(userId, name);
        entity.setStatus(Status.COMPLETE);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final TaskGraph entity = findOneById(userId, id);
        entity.setStatus(Status.IN_PROGRESS);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final TaskGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.IN_PROGRESS);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) return;
        @NotNull final TaskGraph entity = findOneByName(userId, name);
        entity.setStatus(Status.IN_PROGRESS);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final TaskGraph entity = findOneById(userId, id);
        entity.setName(name);
        entity.setDescription(description);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (name == null) throw new EmptyNameException();
        @NotNull final TaskGraph entity = findOneByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        ITaskRepositoryGraph taskRepositoryGraph = getTaskRepositoryGraph();
        try {
            taskRepositoryGraph.begin();
            taskRepositoryGraph.update(entity);
            taskRepositoryGraph.commit();
        } catch (@NotNull final Exception e) {
            taskRepositoryGraph.rollback();
            throw e;
        } finally {
            taskRepositoryGraph.close();
        }
    }
}
