package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.configuration.ClientConfiguration;
import ru.vartanyan.tm.marker.IntegrationCategory;

public class UserEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final UserEndpoint userEndpoint = context.getBean(UserEndpoint.class);

    private Session session;

    private SessionDTO sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        UserDTO user1 = new UserDTO();
        user1.setLogin("BB");
        userEndpoint.removeUserByItsLogin("BB", sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLogin() {
        UserDTO user = new UserDTO();
        user.setLogin("AA");
        final String login = user.getLogin();
        final UserDTO userFound = userEndpoint.findUserByLogin(login, sessionAdmin);
        //Assert.assertEquals("AA", userFound.getLogin());
        Assert.assertEquals("AA", userFound.getLogin());
    }

}
