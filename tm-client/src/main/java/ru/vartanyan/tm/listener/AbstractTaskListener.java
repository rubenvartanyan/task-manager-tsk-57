package ru.vartanyan.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vartanyan.tm.endpoint.AdminEndpoint;
import ru.vartanyan.tm.endpoint.TaskEndpoint;

public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected AdminEndpoint adminEndpoint;

}
