package ru.vartanyan.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractListener;

@Component
public class AboutListener extends AbstractListener {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public @NotNull String name() {
        return "show-about";
    }

    @Override
    public String description() {
        return "Show about developer info";
    }

    @Override
    @EventListener(condition = "@aboutListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[ABOUT]");
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("developerEmail"));
    }

}
